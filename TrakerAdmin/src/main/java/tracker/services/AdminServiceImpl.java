package tracker.services;

import tracker.entity.Pack;
import tracker.entity.Route;
import tracker.repository.PackageRepo;
import tracker.repository.PackageRepoInterface;
import tracker.repository.RouteRepo;
import tracker.repository.RouteRepoInterface;

import javax.jws.WebService;

@WebService(endpointInterface = "tracker.services.AdminService")
public class AdminServiceImpl implements AdminService {

    PackageRepoInterface packRepo = new PackageRepo();
    RouteRepoInterface routeRepo =new RouteRepo();
    @Override
    public boolean addRoute(Route r) {
     return routeRepo.addRoute(r);
    }

    @Override
    public boolean addPackage(Pack pack) {
        pack.setTracking(false);
        return packRepo.addPackage(pack);
    }

    @Override
    public boolean removePackage(int id) {
        return packRepo.removePackage(id);
    }

    @Override
    public boolean registerForTracking(int id) {
        return packRepo.registerForTracking(id);
    }
}
