package tracker.repository;

import tracker.entity.Route;

public interface RouteRepoInterface {

    boolean addRoute(Route r);
}
