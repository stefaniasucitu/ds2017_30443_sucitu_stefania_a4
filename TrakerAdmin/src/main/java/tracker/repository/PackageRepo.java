package tracker.repository;

import org.hibernate.Session;
import org.hibernate.Transaction;
import tracker.entity.Pack;
import utilHibernate.MyHibernateUtil;

import java.util.List;



public class PackageRepo implements PackageRepoInterface{

    @Override
    public Pack getPackById(int id)  {
        Session session = MyHibernateUtil.getSessionFactory().openSession();
        boolean result = false;
        List<Pack> packsList = null;

        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
             packsList =  session.createQuery("FROM Pack ").list();
            tx.commit();
            if (packsList!= null) result = true;
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        for( Pack pa : packsList){
            if(pa.getId()==id){
                return pa;
            }
        }
       return null;
    }

    @Override
    public boolean addPackage(Pack pack) {
        Session session = MyHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            session.save(pack);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public boolean removePackage(int id) {
        Session session = MyHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Pack pack= getPackById(id);
        try {
            tx = session.getTransaction();
            tx.begin();
            session.delete(pack);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public boolean registerForTracking(int id) {
        Session session = MyHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Pack pack= getPackById(id);
        pack.setTracking(true);
        try {
            tx = session.getTransaction();
            tx.begin();
            session.update(pack);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }
}
