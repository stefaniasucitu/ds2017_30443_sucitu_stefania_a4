package tracker.repository;

import tracker.entity.Pack;

public interface PackageRepoInterface {
    Pack getPackById(int id);
    boolean addPackage(Pack pack);
    boolean removePackage(int id);
    boolean registerForTracking(int id);
}
