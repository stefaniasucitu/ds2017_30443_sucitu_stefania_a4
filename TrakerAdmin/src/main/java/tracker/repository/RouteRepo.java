package tracker.repository;

import org.hibernate.Session;
import org.hibernate.Transaction;
import tracker.entity.Route;
import utilHibernate.MyHibernateUtil;



public class RouteRepo implements RouteRepoInterface {

    @Override
    public boolean addRoute(Route r) {
        Session session = MyHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            session.save(r);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }
}
