package tracker.entity;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="route")
public class Route implements java.io.Serializable{
   private int routeID;
    private Pack packageID;
    private String city;
    private int time;

    public Route() {
    }

    public Route(Pack packageID, String city, int time) {
        this.packageID=packageID;
        this.city = city;
        this.time = time;
    }
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name="ROUTE_ID", unique= true,nullable =false)
    public int getRouteID() {
        return routeID;
    }

    public void setRouteID(int routeID) {
        this.routeID = routeID;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PACKAGE_ID", nullable = false)
    public Pack getPackageID() {
        return packageID;
    }

    public void setPackageID(Pack packageID) {
        this.packageID = packageID;
    }

    @Column(name="CITY")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    @Column(name="TIME")
    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Route{" +
                "routeID=" + routeID +
                ", packageID=" + packageID+
                ", city='" + city + '\'' +
                ", time=" + time +
                '}';
    }
}
