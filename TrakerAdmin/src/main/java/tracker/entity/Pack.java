package tracker.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="package")
public class Pack implements java.io.Serializable {
    private int id;
    private User sender;
    private User receiver;
    private String name;
    private String description;
    private String senderCity;
    private String receiverCity;
    private  Boolean Tracking;

    private Set<Route> routes=new HashSet<Route>(0);
    public Pack() {
    }

    public Pack(User sender, User receiver, String name, String description, String senderCity, String receiverCity, Boolean tracking) {
        this.sender = sender;
        this.receiver = receiver;
        this.name = name;
        this.description = description;
        this.senderCity = senderCity;
        this.receiverCity = receiverCity;
        Tracking = tracking;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name="PACKAGE_ID", unique= true,nullable =false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SENDER", nullable = false)
    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "RECEIVER", nullable = false)
    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    @Column(name="PACKAGE_NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Column(name="PACKAGE_DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name="SENDER_CITY")
    public String getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }

    @Column(name="RECEIVER_CITY")
    public String getReceiverCity() {
        return receiverCity;
    }

    public void setReceiverCity(String receiverCity) {
        this.receiverCity = receiverCity;
    }

    @Column(name="TRACKING")
    public Boolean getTracking() {
        return Tracking;
    }

    public void setTracking(Boolean tracking) {
        Tracking = tracking;
    }


    @OneToMany(fetch = FetchType.EAGER)
    public Set<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(Set<Route> routes) {
        this.routes = routes;
    }

    @Override
    public String toString() {
        return "Pack{" +
                "id=" + id +
                ", sender=" + sender +
                ", receiver=" + receiver +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", senderCity='" + senderCity + '\'' +
                ", receiverCity='" + receiverCity + '\'' +
                ", Tracking=" + Tracking + '}';
    }
}
