package interfaces;

import entities.Pack;
import entities.Route;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;



@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface AdminServiceInterface
{
    @WebMethod
    boolean addRoute(Route r);
    @WebMethod
    boolean addPackage(Pack pack);
    @WebMethod
    boolean removePackage(int id);
    @WebMethod
    boolean registerForTracking(int id);
}
