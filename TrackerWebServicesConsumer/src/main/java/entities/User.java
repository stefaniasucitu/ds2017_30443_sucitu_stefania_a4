package entities;

import java.util.HashSet;
import java.util.Set;


public class User implements java.io.Serializable {
    private int id;
    private String username;
    private String password;
    private String role;

    private Set<Pack> userPacks = new HashSet<Pack>(0);
    public User() {
    }

    public User(String username, String password, String role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    public Set<Pack> getUserPacks(){
        return this.userPacks;
    }

    public void setUserPacks(Set<Pack> userPacks) {
        this.userPacks = userPacks;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", userPacks=" + userPacks +
                '}';
    }
}
