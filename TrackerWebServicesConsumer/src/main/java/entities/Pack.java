package entities;

import java.util.HashSet;
import java.util.Set;


public class Pack implements java.io.Serializable {
    private int id;
    private User sender;
    private User receiver;
    private String name;
    private String description;
    private String senderCity;
    private String receiverCity;
    private  Boolean Tracking;
    private Set<Route> routes = new HashSet<Route>(0);

    public Pack() {
    }

    public Pack(User sender, User receiver, String name, String description, String senderCity, String receiverCity, Boolean tracking) {
        this.sender = sender;
        this.receiver = receiver;
        this.name = name;
        this.description = description;
        this.senderCity = senderCity;
        this.receiverCity = receiverCity;
        Tracking = tracking;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }


    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }


    public String getReceiverCity() {
        return receiverCity;
    }

    public void setReceiverCity(String receiverCity) {
        this.receiverCity = receiverCity;
    }


    public Boolean getTracking() {
        return Tracking;
    }

    public void setTracking(Boolean tracking) {
        Tracking = tracking;
    }


    public Set<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(Set<Route> routes) {
        this.routes = routes;
    }

    @Override
    public String toString() {
        return "Pack{" +
                "id=" + id +
                ", sender=" + sender.getId() +
                ", receiver=" + receiver.getId() +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", senderCity='" + senderCity + '\'' +
                ", receiverCity='" + receiverCity + '\'' +
                ", Tracking=" + Tracking + '}';
    }
}
