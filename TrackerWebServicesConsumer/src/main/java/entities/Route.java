package entities;

import java.sql.Time;


public class Route implements java.io.Serializable{
   private int routeID;
    private Pack packageID;
    private String city;
    private Time time;

    public Route() {
    }

    public Route(Pack packageID,String city, Time time) {
        this.packageID=packageID;
        this.city = city;
        this.time = time;
    }

    public int getRouteID() {
        return routeID;
    }

    public void setRouteID(int routeID) {
        this.routeID = routeID;
    }

    public Pack getPackageID() {
        return packageID;
    }

    public void setPackageID(Pack packageID) {
        this.packageID = packageID;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }



    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Route{" +
                "routeID=" + routeID +
                ", packageID=" + packageID.getId()+
                ", city='" + city + '\'' +
                ", time=" + time +
                '}';
    }
}
