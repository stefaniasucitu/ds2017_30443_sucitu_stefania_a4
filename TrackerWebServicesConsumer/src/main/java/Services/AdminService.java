package Services;

import interfaces.AdminServiceInterface;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class AdminService {
    private Service adminService;
    private AdminServiceInterface adminServ;

    public  AdminService() throws MalformedURLException {
        URL wsdlURL = new URL("http://localhost:8080/ws/admin?wsdl");
        //check above URL in browser, you should see WSDL file

        //creating QName using targetNamespace and name
        QName qname = new QName("http://services.tracker/", "AdminServiceImplService");

       adminService = Service.create(wsdlURL, qname);

        //We need to pass interface and model beans to client
         adminServ = adminService.getPort(qname, AdminServiceInterface.class);

    }

    public Service getAdminService() {
        return adminService;
    }

    public void setAdminService(Service adminService) {
        this.adminService = adminService;
    }

    public AdminServiceInterface getAdminServ() {
        return adminServ;
    }

    public void setAdminServ(AdminServiceInterface adminServ) {
        this.adminServ = adminServ;
    }
}
