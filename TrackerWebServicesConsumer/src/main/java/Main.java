import Services.AdminService;
import interfaces.AdminServiceInterface;

import java.net.MalformedURLException;

public class Main {

    public static void main(String[] args) throws MalformedURLException {
        AdminService adminService = new AdminService();
        AdminServiceInterface service = adminService.getAdminServ();
        System.out.println(service.registerForTracking(3));
    }
}
